@extends('index.layout')

@section('content')

<div class="slider-main">
  <div class="prev">
    <p class="arrow"><span class="fa fa-arrow-left"></span></p>
  </div>
  <div class="next">
    <p class="arrow"><span class="fa fa-arrow-right"></span></p>
  </div>
  <div class="siema">
    <div class="slide">
      <div class="image-container" id="slider-big-photo">
        <img src="/assets/index/img/KitchenSlide.jpg" alt="Siema image" />
      </div>
      <div class="image-container" id="slider-small-photo">
        <img src="/assets/index/img/5-min.jpg" alt="Siema image">
      </div>      
      <a href="/catalog?category_id=2">Подробнее</a>  
    </div>
    <div class="slide">
      <div class="image-container" id="slider-big-photo">
        <img src="/assets/index/img/ClosetSlide.jpg" alt="Siema image" />
      </div>
      <div class="image-container" id="slider-small-photo">
        <img src="/assets/index/img/3-min.jpg" alt="Siema image">
      </div>         
      <a href="/catalog?category_id=4">Подробнее</a>
    </div>    
    <div class="slide">
      <div class="image-container" id="slider-big-photo">
        <img src="/assets/index/img/WardrobeSlide.jpg" alt="Siema image" />
      </div>
      <div class="image-container" id="slider-small-photo">
        <img src="/assets/index/img/4-min.jpg" alt="Siema image">
      </div>         
      <a href="/catalog?category_id=3">Подробнее</a> 
    </div>
    <div class="slide">
      <div class="image-container" id="slider-big-photo">
          <img src="/assets/index/img/ChildSlide.jpg" alt="Siema image" />
      </div>
      <div class="image-container" id="slider-small-photo">
        <img src="/assets/index/img/2_2-min.jpg" alt="Siema image">
      </div>         
      <a href="/catalog?category_id=1">Подробнее</a> 
    </div>
    <div class="slide">
      <div class="image-container" id="slider-big-photo">
        <img src="/assets/index/img/CommerSlide.jpg" alt="Siema image" />
      </div>
      <div class="image-container" id="slider-small-photo">
        <img src="/assets/index/img/1-min.jpg" alt="Siema image">
      </div>         
      <a href="/catalog?category_id=5">Подробнее</a> 
    </div>        
  </div>
  <div class="slider-buttons">
    <button class="btn0 siema-btn active" id="btn0"></button>
    <button class="btn1 siema-btn" id="btn1"></button>
    <button class="btn2 siema-btn" id="btn2"></button>
    <button class="btn3 siema-btn" id="btn3"></button>
    <button class="btn4 siema-btn" id="btn4"></button>
  </div>
</div>

<div class="container">
  <h3 class="title">
    <img src="/assets/index/img/mebel-min.png" alt=""><p>Выбор мебели <br> по категориям</p>
  </h3>
  <div class="categories">
    <div>
      <img src="/assets/index/img/c1-min.png" alt="">
      <h2>Кухни</h2>
      <a href="/catalog?category_id=2">Показать все</a>
    </div>
    <div>
      <img src="/assets/index/img/belii_chernii_3_kopia-min.png" alt="">
      <h2>Шкафы-купе</h2>
      <a href="/catalog?category_id=4">Показать все</a>
    </div>
    <div>
      <img src="/assets/index/img/c3-min.png" alt="">
      <h2>Гардеробные</h2>
      <a href="/catalog?category_id=3">Показать все</a>
    </div>
    <div>
      <img src="/assets/index/img/c4-min.png" alt="">
      <h2>Детская мебель</h2>
      <a href="/catalog?category_id=1">Показать все</a>
    </div>
    <div>
      <img src="/assets/index/img/CommercialFurniture-min.png" alt="">
      <h2>Торговая мебель</h2>
      <a href="/catalog?category_id=5">Показать все</a>      
    </div>
  </div>
</div>
<div class="container">
  <h3 class="title">
    <img src="/assets/index/img/popular-min.png" alt=""><p>Популярные<br> товары</p>
  </h3>
  <div class="container popular list-item1">

    <a href="/{{ $popular[0]->id }}">
      <div class="item">
        @if($popular[0]->is_by_order)
        <div class="stick2">Под заказ</div>
        @endif
        <img src="{{$popular[0]->image_url}}" alt="">
        <p class="name">{{ $popular[0]->name }}</p>
        <span class="get-item" href="#" data-popup-open="popup-{{ $popular[0]->id }}">Заказать</span>
      </div>
    </a>
    <div class="popup" data-popup="popup-{{ $popular[0]->id }}">
      <div class="popup-inner">
        <h2 class="popUpTitle">Вы заказываете {{ $popular[0]->name }}</h2>
        <p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
        <form class="contactUs" role="form" method="post" action="/send">
          <input style="display: none;" name="product" type="text" value="{{ $popular[0]->name }}"/>
          <input name="name" type="text" placeholder="Имя"required>
          <input name="email" type="text" placeholder="Email">
          <input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
          <textarea name="text" placeholder="Cообщение"></textarea>
          <button type="submit">Заказать</button>
        </form>
        <a class="popup-close" data-popup-close="popup-{{ $popular[0]->id }}" href="#">x</a>
      </div>
    </div>

    <a href="/{{ $popular[1]->id }}">
      <div class="item">
        @if($popular[1]->is_by_order)
        <div class="stick2">Под заказ</div>
        @endif
        <img src="{{$popular[1]->image_url}}" alt="">
        <p class="name">{{ $popular[1]->name }}</p>
        <span class="get-item" href="#" data-popup-open="popup-{{ $popular[1]->id }}">Заказать</span>
      </div>
    </a>
    <div class="popup" data-popup="popup-{{ $popular[1]->id }}">
      <div class="popup-inner">
        <h2 class="popUpTitle">Вы заказываете {{ $popular[1]->name }}</h2>
        <p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
        <form class="contactUs" role="form" method="post" action="/send">
          <input style="display: none;" name="product" type="text" value="{{ $popular[1]->name }}"/>
          <input name="name" type="text" placeholder="Имя" required>
          <input name="email" type="text" placeholder="Email">
          <input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
          <textarea name="text" placeholder="Cообщение"></textarea>
          <button type="submit">Заказать</button>
        </form>
        <a class="popup-close" data-popup-close="popup-{{ $popular[1]->id }}" href="#">x</a>
      </div>
    </div>

    <a href="/{{ $popular[2]->id }}">
      <div class="item">
        @if($popular[2]->is_by_order)
        <div class="stick2">Под заказ</div>
        @endif
        <img src="{{$popular[2]->image_url}}" alt="">
        <p class="name">{{ $popular[2]->name }}</p>
        <span class="get-item" href="#" data-popup-open="popup-{{ $popular[2]->id }}">Заказать</span>
      </div>
    </a>
    <div class="popup" data-popup="popup-{{ $popular[2]->id }}">
      <div class="popup-inner">
        <h2 class="popUpTitle">Вы заказываете {{ $popular[2]->name }}</h2>
        <p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
        <form class="contactUs" role="form" method="post" action="/send">
          <input style="display: none;" name="product" type="text" value="{{ $popular[2]->name }}"/>
          <input name="name" type="text" placeholder="Имя" required>
          <input name="email" type="text" placeholder="Email">
          <input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
          <textarea name="text" placeholder="Cообщение"></textarea>
          <button type="submit">Заказать</button>
        </form>
        <a class="popup-close" data-popup-close="popup-{{ $popular[2]->id }}" href="#">x</a>
      </div>
    </div>

    <a href="/{{ $popular[3]->id }}">
      <div class="item">
        @if($popular[3]->is_by_order)
        <div class="stick2">Под заказ</div>
        @endif
        <img src="{{$popular[3]->image_url}}" alt="">
        <p class="name">{{ $popular[3]->name }}</p>
        <span class="get-item" href="#" data-popup-open="popup-{{ $popular[3]->id }}">Заказать</span>
      </div>
    </a>
    <div class="popup" data-popup="popup-{{ $popular[3]->id }}">
      <div class="popup-inner">
        <h2 class="popUpTitle">Вы заказываете {{ $popular[3]->name }}</h2>
        <p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
        <form class="contactUs" role="form" method="post" action="/send">
          <input style="display: none;" name="product" type="text" value="{{ $popular[3]->name }}"/>
          <input name="name" type="text" placeholder="Имя" required>
          <input name="email" type="text" placeholder="Email">
          <input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
          <textarea name="text" placeholder="Cообщение"></textarea>
          <button type="submit">Заказать</button>
        </form>
        <a class="popup-close" data-popup-close="popup-{{ $popular[3]->id }}" href="#">x</a>
      </div>
    </div>

  </div>
</div>

<div class="container mt-40 mobilehide">
  <h3 class="title">
    <p>Преимущества</p>
  </h3>
  <div class="flex-container benefits">
    <div class="benefit">
      <img src="/assets/index/img/p1-min.png" alt="">
      <p class="text">Собственное <br> производство</p>
    </div>
    <div class="next">
      <img src="/assets/index/img/p_next-min.png" alt="">
    </div>
    <div class="benefit">
      <img src="/assets/index/img/p2-min.png" alt="">
      <p class="text">Высококвалифи-<br>цированные специалисты</p>
    </div>
    <div class="next">
      <img src="/assets/index/img/p_next-min.png" alt="">
    </div>
    <div class="benefit">
      <img src="/assets/index/img/p3-min.png" alt="">
      <p class="text">Современное <br> оборудование</p>
    </div>
    <div class="next">
      <img src="/assets/index/img/p_next-min.png" alt="">
    </div>
    <div class="benefit">
      <img src="/assets/index/img/p4-min.png" alt="">
      <p class="text">Весь комплекс <br> работ</p>
    </div>
    <div class="next">
      <img src="/assets/index/img/p_next-min.png" alt="">
    </div>
    <div class="benefit">
      <img src="/assets/index/img/p5-min.png" alt="">
      <p class="text">Адекватные <br> цены</p>
    </div>
  </div>
</div>

<div class="container">
  <h3 class="title mobilehide">
    <img src="/assets/index/img/news-min.png" alt=""><p>Последние<br> новости</p>
  </h3>
  <div class="flex-container news mobilehide">
  @foreach($lastNews as $lastArticle)
  <div>
    <a href="/news/{{$lastArticle->slug}}">
      <img src="{{$lastArticle->image_url}}" alt="">
      <p class="title">{{$lastArticle->name}}</p>
      <p class="data">{{$lastArticle->created_at->format('d.m.Y')}}</p>
      <p class="info">{{$lastArticle->short_info}}</p>
    </a>
  </div>
  @endforeach
  </div>
</div>

<div style="position: relative;">
  <div class="container mt-40 mobilehide">
    <h3 class="title">
      <p>Схема работы с нами</p>
    </h3>
    <div class="flex-container works">
      <div class="work">
        <img src="/assets/index/img/w1-min.png" alt="">
        <div class="hr"></div>
        <p class="text">1. Замер</p>
      </div>
      <div class="work">
        <img src="/assets/index/img/w2-min.png" alt="">
        <div class="hr"></div>
        <p class="text">2. Утверждение <br> стоимости</p>
      </div>
      <div class="work">
        <img src="/assets/index/img/w3-min.png" alt="">
        <div class="hr"></div>
        <p class="text">3. Заключение <br> договора</p>
      </div>
      <div class="work">
        <img src="/assets/index/img/w4-min.png" alt="">
        <div class="hr"></div>
        <p class="text">4. Изготовление</p>
      </div>
      <div class="work">
        <img src="/assets/index/img/w5-min.png" alt="">
        <div class="hr"></div>
        <p class="text">5. Установка</p>
      </div>
    </div>  
  </div>
  <div class="bighr"></div>
</div>

<div class="container mt-80">
  <div class="paragraph">
    <h4>Кухни под заказ в Минске</h4>
    <p>Уникальный дизайн с адаптацией под любую планировку, экологически чистые и долговечные материалы, а также качественная работа опытных мастеров, все это – кухни под заказ в Минске от Фабрики CLEVER. Корпусная мебель, которая прослужит много лет, выполненная с учетом всех пожеланий и требований может появиться в Вашей квартире или доме буквально через пару дней! Бесплатное создание дизайн-проекта, гарантия на материалы и сборку, приятные цены и скидки – дополнительные бонусы от сотрудничества с мастерами своего дела!</p>
  </div>
  <div class="paragraph">
    <h4>Кухни под заказ, почему это выгодно</h4>
    <p>Практичность, функциональность и индивидуальность, вот основные факторы грамотного оформления помещений, но представление о том, как нужно воплотить в жизнь эти требования у каждого разные, именно поэтому заказать кухню в Минске недорого правильное решение для каждого кто задумывается о ремонте: <br><br><b>1.</b> Квартиры в новостройках практически всегда со свободной планировкой. Это огромный простор для творчества – можно зонировать пространство как угодно, а значит стоит обратить внимание на кухни на заказ в Минске. Барная стойка, островные тумбы с рабочими поверхностями, или шкафчики под окном – кода нет ограничений, самое время проявлять фантазию! <br> <b>2.</b> Мой дом – моя крепость, владельцам коттеджей особенно повезло – под кухню, столовую и гостиную всегда выделяется максимум места на первом этаже. Учитывая общие затраты на содержание частного дома, купить кухню в Минске недорого – предложение от которого невозможно отказаться! <br><b>3</b>. Если центральный акцент на кухне старинный стол, доставшийся от бабушки или пара плетеных кресел, с которыми не хочется расставаться, самое время собирать кухню под заказ. Опытный дизайнер сможет органично вписать любимые вещи в интерьер и дополнить их свежими решениями, а мастера в свою очередь будут проводить работы максимально аккуратно, чтобы сохранить антиквариат, или просто дорогие сердцу предметы обстановки в первозданном виде! <br><br>Кроме того, сотрудничество с фабрикой кухонь CLEVER это:<br><br> <b>1.</b> Выбор стиля. Мы предлагаем своим клиентам широкий ассортимент корпусной мебели. В большинстве случаев, типовые решения в классике, минимализме или модерне, понравятся далеко не всем хозяевам. Поэтому мы предлагаем у нас купить кухню в Минске под заказ. В этом случае вы получите уникальный фасад, оформленный по вашему желанию!<br> <b>2.</b> Учет планировки. Основная причина заказать кухню в Минске с индивидуальным дизайном – это желание органичной установки в помещении. Благодаря нестандартным конструкционным решениям, вы сможете скрыть инженерные системы, газовые счетчики и котлы. Следовательно, придадите кухне изысканный и цельный внешний вид без лишних элементов.<br> <b>3.</b> В зависимости от сложности заказа, сроки изготовления кухни начинаются от 14 рабочих дней. Готовая кухня от Фабрики CLEVER отличается высокой надежностью и функциональностью. Она идеально впишется в помещение и будет ежедневно радовать вас и ваших близких! Таким образом, время ремонта и затраты нервов сократятся в десятки раз, а цена услуг остается неизменно приятной. К счастью для тех, кто хочет купить кухню в Минске, фото и цены одинаково радуют глаз, а значит, не стоит отказывать себе в удовольствии! Закажите вызов нашего дизайнера и получите желанную кухню при оптимальных финансовых тратах!</p>
  </div>
</div>
<script>
  const mySiema = new Siema({
    duration: 1000, 
    draggable: false, 
    loop: true
  });
  $('#btn0').click(function(event) {
    event.preventDefault();
    $('#btn0').addClass('active');
    $('#btn1').removeClass('active');
    $('#btn2').removeClass('active');
    $('#btn3').removeClass('active');
    $('#btn4').removeClass('active');
  })
  $('#btn1').click(function(event) {
    event.preventDefault();
    $('#btn1').addClass('active');
    $('#btn0').removeClass('active');
    $('#btn2').removeClass('active');
    $('#btn3').removeClass('active');
    $('#btn4').removeClass('active');
  })  
  $('#btn2').click(function(event) {
    event.preventDefault();
    $('#btn2').addClass('active');
    $('#btn1').removeClass('active');
    $('#btn0').removeClass('active');
    $('#btn3').removeClass('active');
    $('#btn4').removeClass('active');
  })
  $('#btn3').click(function(event) {
    event.preventDefault();
    $('#btn3').addClass('active');
    $('#btn1').removeClass('active');
    $('#btn2').removeClass('active');
    $('#btn0').removeClass('active');
    $('#btn4').removeClass('active');
  });
  $('#btn4').click(function(event) {
    event.preventDefault();
    $('#btn4').addClass('active');
    $('#btn3').removeClass('active');
    $('#btn1').removeClass('active');
    $('#btn2').removeClass('active');
    $('#btn0').removeClass('active');
  });
  const btn0 = document.querySelector('.btn0');
  const btn1 = document.querySelector('.btn1');
  const btn2 = document.querySelector('.btn2');
  const btn3 = document.querySelector('.btn3');
  const btn4 = document.querySelector('.btn4');

  btn0.addEventListener('click', () => mySiema.goTo(0));
  btn1.addEventListener('click', () => mySiema.goTo(1));
  btn2.addEventListener('click', () => mySiema.goTo(2));
  btn3.addEventListener('click', () => mySiema.goTo(3));
  btn4.addEventListener('click', () => mySiema.goTo(4));

  const prev = document.querySelector('.prev');
  const next = document.querySelector('.next');
  
  prev.addEventListener('click', function (event) {
    event.preventDefault();
    let curButton = $('.active').attr('id');
    let curImageId = curButton.substring(3);
    let prevImageId = curImageId - 1;
    if(prevImageId < 0) {
      prevImageId = $('.slider-buttons').children().length - 1;
    }
    $('#btn' + curImageId).removeClass('active');
    $('#btn' + prevImageId).addClass('active');
    mySiema.goTo(prevImageId);    
  });

  next.addEventListener('click', function (event) {
    event.preventDefault();
    let curButton = $('.active').attr('id');
    let curImageId = curButton.substring(3);
    let nextImageId = +(curImageId) + 1;
    if(nextImageId >= $('.slider-buttons').children().length) {
      nextImageId = 0;    
    }
    $('#btn' + curImageId).removeClass('active');
    $('#btn' + nextImageId).addClass('active');      
    mySiema.goTo(nextImageId);
  });
  var theInterval;

  function startSlide() {
      theInterval = setInterval(() => $('.next').trigger('click'), 3000);
  }

  function stopSlide() {
      clearInterval(theInterval);
  }
  var prevNextClicked = false;
  $(function () {
    startSlide();
    $('.slider-main').on('mouseover', function () {
      stopSlide();
    });
    $('.slider-main').on('mouseout', function () {
      if (!prevNextClicked) {
        startSlide();
      }
    });
  });
  $(function () {
    $('.siema-btn').click(function () {
      prevNextClicked = true;
    });
  });
</script>
@endsection