@extends('index.layout')

@section('content')
<div class="container mt-40">
	<div class="left-menu">
		<a href="/catalog?category_id={{$product->category_id}}"><span class="label-name"><span class="fa fa-mail-reply-all"></span> Вернуться в раздел</span></a>
		<a href="" id="goToOverview"><span class="label-name"><span class="fa fa-search"></span> Обзор товара</span></a>
		<a href="" id="goToCharacteristics"><span class="label-name"><span class="fa fa-arrows"></span> Характеристики</span></a>
		<a href="" id="goToSameProducts"><span class="label-name"><span class="fa fa-clone"></span> Похожие товары</span></a>
	</div>
	<div class="inner-slider" id="overview">
		<div class="mob">
			<div style="text-align: center; margin-bottom: 10px;" class="title-of-info">
				<b>{{$product->name}}</b>
			</div>
			<div style="text-align: center; margin-bottom: 20px;" class="descr">
				{{$product->short_info}}
			</div>
		</div>
		<div class="big-photo">
			<div class="siema">
				@foreach($gallery as $photo)
				<!-- <a id="photo{{ $photo->id }}" name="{{ $photo->image_url }}" href="" data-popup-open="approx-image"> -->
					<img src="{{$photo->image_url}}" alt="" data-popup-open="approx-image" id="photo{{ $photo->id }}">
				<!-- </a> -->
				@endforeach
			</div>
		</div>
		<div class="small-photo">
			@foreach($gallery as $photo)
			<img src="{{$photo->image_url}}" alt="">
			@endforeach			
		</div>
		<div class="table-head">
			<a href="" class="active" id="characteristics">Характеристики</a>
			<a href="" id="description">Описание</a>
		</div>	
		<div class="table-body" id="inDescription" style="display: none">
			<div class="table-element">
				<div class="inner-descr">
					{!! $product->description !!}
				</div>
			</div>
		</div>	
		<div class="table-body" id="inCharacteristics">
			@foreach($characteristics as $char) 
				<div class="table-element"> 
					<div class="name"> 
					{{$char->name}} 
					</div> 
					<div class="composition"> 
					{{$char->options}} 
					</div> 
				</div> 
			@endforeach 		
		</div>			
	</div>
	<div class="about-product">
		<div class="title-of-info">
			<b>{{$product->name}}</b>
		</div>
		<div class="descr">
			{{$product->short_info}}
		</div>
		<div class="choose-size">
			<span class="fa fa-check"></span>Изготовим под ваши размеры
		</div>
		<hr>
		<div class="characteristics">
			<div><p>Длина</p></div>
			<div class="line"></div>
			<select disabled name="">
				<option value="1">Любая</option>
			</select>
		</div>
		<div class="characteristics">
			<div><p>Ширина</p></div>
			<div class="line"></div>
			<select disabled name="">
				<option value="1">Любая</option>
			</select>			
		</div>		
		<div class="characteristics">
			<div><p>Высота</p></div>
			<div class="line"></div>
			<select disabled name="">
				<option value="1">Любая</option>
			</select>			
		</div>
		@foreach($characteristics as $char)
		<div class="characteristics">
			<div><p>{{$char->name}}</p></div>
			<select id="char{{$char->id}}" name="{{$char->name}}">
				@if(count($char->options_array) > 0)
					@foreach($char->options_array as $descr)
			    		<option value="{{$descr}}">{{$descr}}</option>
			    	@endforeach
			    @else
			    	<option id="needInDisabling" value="">Любой</option>
			    @endif
			</select>				
		</div>
		@endforeach
		<div class="number-with-plus-minus">
		</div>	
		<button id="fillchar" class="order" data-popup-open="popup-char">
			Заказать
		</button>
		<div class="info-message">
			Дизайн
			@if($product->category->id == 5) 
			торговой мебели
			@endif 			
			@if($product->category->id == 4) 
			шкафа
			@endif 
			@if($product->category->id == 3) 
			гардеробной
			@endif
			@if($product->category->id == 2) 
			кухни
			@endif
			@if($product->category->id == 1) 
			детские
			@endif 			 			 			
			обсудит с Вами наш дизайнер, чтобы уточнить детали проекта, согласовать цену, доставку и сроки сборки.
		</div>		
	</div>
</div>
<div class="flex-container" id="products">
	<div class="title2">
  		Похожие товары
  	</div>	
</div>
<div class="container list-item">  		
	@foreach ($products as $product)
		<a class="inner-a" href="/{{ $product->id }}"> 
			<div class="item">
				@if($product->is_by_order)
				<div class="stick2">Под заказ</div>
				@endif
				<div class="img-container">
					<img src="{{$product->image_url}}" alt="">
				</div>
				<p class="name">{{ $product->name }}</p>
				<span class="get-item" href="#" data-popup-open="popup-{{ $product->id }}">Заказать</span>
			</div>
		</a>
		<div class="popup" data-popup="popup-{{ $product->id }}">
			<div class="popup-inner">
				<h2 class="popUpTitle">Вы заказываете {{ $product->name }}</h2>
				<p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
				<form class="contactUs" role="form" method="post" action="/send">
					<input style="display: none;" name="product" type="text" value="{{ $product->name }}"/>
					<input name="name" type="text" placeholder="Имя" required>
					<input name="email" type="text" placeholder="Email">
					<input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
					<textarea name="text" placeholder="Cообщение"></textarea>
					<button type="submit">Заказать</button>
				</form>
				<a class="popup-close" data-popup-close="popup-{{ $product->id }}" href="#">x</a>
			</div>
		</div>
	@endforeach	  		
</div>

<div class="popup" data-popup="popup-char">
	<div class="popup-inner">
		<h2 class="popUpTitle">Заказать {{$product->name}}</h2>
		<p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
		<form class="contactUs" role="form" method="post" action="/send">
			<input style="display:none;" name="product" type="text" value="{{$product->name}}"/>
			<input name="name" type="text" placeholder="Имя" required>
			<input name="email" type="text" placeholder="Email">
			<input name="number" type="tel" value="+375" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
			<textarea name="text" placeholder="Cообщение"></textarea>
			<textarea style="display:none;" id="char" name="char" placeholder="Cообщение"></textarea>
			<button type="submit">Заказать</button>
		</form>
		<a class="popup-close" data-popup-close="popup-char" href="#">x</a>
	</div>
</div>

<div class="popup" data-popup="approx-image">
	<div class="popup-inner" id="insert-photo">
		<img src="" alt="">
		<a class="popup-close" data-popup-close="approx-image" href="#">x</a>
	</div>
</div>

	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/index/js/siema.min.js"></script>
	<script>
		$('#description').click(function(event) {
			event.preventDefault();
			$('#characteristics').removeClass('active');
			$('#description').addClass('active');
			$('#inDescription').css('display', 'block');
			$('#inCharacteristics').css('display', 'none');

		});
		$('#characteristics').click(function(event) {
			event.preventDefault();
			$('#characteristics').addClass('active');
			$('#description').removeClass('active');
			$('#inDescription').css('display', 'none');
			$('#inCharacteristics').css('display', 'block');			

		});
		const siema = new Siema({duration: 1000});
		const siemaKids = $('.small-photo').children();
		for (let i = 0; i < siemaKids.length; i++) {
			siemaKids[i].addEventListener('click', () => siema.goTo(i));
		}
		$('#goToOverview').click(function(event) {
			event.preventDefault();
			let destination = $('#overview').position().top;
			console.log(destination);
			$('HTML, BODY').animate({ scrollTop: destination }, 1000);
		});
		$('#goToCharacteristics').click(function(event) {
			event.preventDefault();
			let destination = $('#characteristics').position().top;
			console.log(destination);
			$('HTML, BODY').animate({ scrollTop: destination }, 1000);	
		});
		$('#goToSameProducts').click(function(event) {
			event.preventDefault();
			let destination = $('#products').position().top;
			console.log(destination);
			$('HTML, BODY').animate({ scrollTop: destination }, 1000);
		});	
		$("#fillchar").click(function(event) {
			event.preventDefault();
			var sites = {!! json_encode($characteristics) !!};
			for (key in sites){
				$("#char")[0].value +='<p><b>' + sites[key].name + ': </b> ' + $("#char" + sites[key].id)[0].value + '</p>';
			}
		});
		$('HTML, BODY').click(function (event) {
			let targetId = event.target.id;
			console.log('in');
			if(targetId.indexOf('photo') !== -1) {
				let src = $('#' + targetId).attr('src');
				$('#insert-photo img').attr('src', src);
			}
		});		
	</script>
@endsection