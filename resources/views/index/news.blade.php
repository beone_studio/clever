@extends('index.layout')

@section('content')
<div class="flex-container">
	<div class="title2">
		Новости
	</div>
</div>
<div class="container news">
  @foreach($news as $article)
    <div class="news-item">
      <a href="/news/{{$article->slug}}">
        <img src="{{$article->image_url}}" alt="">
        <p class="title">{{$article->name}}</p>
        <p class="data">{{$article->created_at->format('d.m.Y')}}</p>
        <p class="info">{{$article->short_info}}</p>
      </a>
    </div>
  @endforeach
</div>
@endsection