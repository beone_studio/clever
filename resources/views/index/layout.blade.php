<!DOCTYPE html>
<html>
  <head>
    <title>Clever</title>
    <link rel="stylesheet" href="/assets/index/css/main.css">
    <link rel="stylesheet" href="/assets/index/css/catalog.css">
		<link rel="stylesheet" href="/assets/index/css/media.css">
		<link rel="stylesheet" href="/assets/index/css/mobile_media.css">
    <link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="shortcut icon" href="/assets/index/img/favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/index/js/siema.min.js"></script>
  </head>
<body>
	<div id="mob-menu">
		<ul>
			<a class="popup-close" id="hide-menu" href="#"><span style="margin-top: 2px;" class="fa fa-arrow-left"></span></a>
			<li><a class="top-a" href="/">Главная</a></li>
			<li><a class="top-a" href="/catalog?category_id=2">Кухни</a></li>
			<li><a class="top-a" href="/catalog?category_id=4">Шкафы-купе</a></li>
			<li><a class="top-a" href="/catalog?category_id=3">Гардеробные</a></li>
			<li><a class="top-a" href="/catalog?category_id=1">Детская мебель</a></li>
			<li><a class="top-a" href="/catalog?category_id=5">Торговая мебель</a></li>
			<li><a class="top-a" href="/news">Новости</a></li>
			<li><a class="top-a" data-popup-open="popup-main" href="#">Заказать</a></li>
		</ul>
	</div>	
	<header>
	  @if(Session::has('flash_message'))
	  <div id="flash_message">
	    <span class="fa fa-check" style="margin-right: 10px;font-size: 24px;"></span>{{ Session::get('flash_message') }}
	  </div>
	  @endif

	  <div class="panel">
		 	<div class="container">
		 		<a class="top-a" href="/">Главная</a>
				<a class="top-a" href="/catalog?category_id=4">Каталог</a>
				<a class="top-a" href="/news">Новости</a>
	  		<a class="top-a" data-popup-open="popup-main" href="#">Заказать</a>
		 	</div>
	  </div>
	  <div class="flex-container">
			<div class="mobile-menu">
				<span class="fa fa-bars"></span>
			</div>
	  	<div class="logo">
				<a href='/'>
					<img src="/assets/index/img/logo-min.png" alt="">
				</a>
	      <p class="placetip">Работаем по Минcку<br>и Минской области</p>
	    </div>
	    <div class="geoPos">
	    	<img src="/assets/index/img/1-min.png" alt="">
	    	<tip>Ваш город</tip>
	    	<p class="geo" id="result"></p>
	    </div>
		<div class="vert-align">
			<div class="phone">
		      <div class="icon-with-shadow">
		        <div class="align_center_to_left">
		          <div class="align_center_to_right"><span class="fa fa-phone" style="font-size: 24px"></span></div>
		        </div>
		      </div>
		      <div class="info">
		        <p class="tip">Ежедневно с 9:00 по 20:00</p>
		        <p class="number">+375 (29) 109 76 94</p>
		        <a data-popup-open="popup-call" href="#">Заказать звонок</a>
		      </div>      
		    </div>
		    <div class="phone">
		      <div class="icon-with-shadow">
		        <div class="align_center_to_left">
		          <div class="align_center_to_right"><span class="fa fa-envelope" style="font-size: 24px"></span></div>
		        </div>
		      </div>
		      <div class="info">
		        <p class="tip">Эл. почта</p>
		        <p class="number">cleverbelarus@gmail.com</p>
		        <a href="#" data-popup-open="popup-email">Написать нам</a>
		      </div>      
		    </div>
		</div>
	  </div>
	</header>
	<div class="panel mob">
			<div class="dropdown-panel">
				<button class="dropbtn">Категории <span class="fa fa-caret-down"></span></button>
			  	<div id="myDropdown" class="dropdown-content">
			    	<a href="/catalog?category_id=2">Кухни</a>
						<a href="/catalog?category_id=4">Шкафы-купе</a>
						<a href="/catalog?category_id=3">Гардеробные</a>
						<a href="/catalog?category_id=1">Детская мебель</a>
						<a href="/catalog?category_id=5">Торговая мебель</a>
			  	</div>
			</div>
			<!-- <span>Test</span>	 -->
	</div>
	<div class="panel">
	  <div class="flex-container">
			<a class="tab" href="/catalog?category_id=2">Кухни</a>
			<a class="tab" href="/catalog?category_id=4">Шкафы-купе</a>
			<a class="tab" href="/catalog?category_id=3">Гардеробные</a>
			<a class="tab" href="/catalog?category_id=1">Детская мебель</a>
			<a class="tab" href="/catalog?category_id=5">Торговая мебель</a>
	  </div>
	</div>
    <div id="content">
        @yield('content')
    </div>

	<footer>
	  <div class="flex-container">

	    <div class="left">
	      <div>
	        <img src="/assets/index/img/logoF-min.png" alt="">
	        <div class="soc-network">
	          <p>Мы в социальных сетях</p>
	          <div>
	            <a href="https://www.facebook.com/groups/913422532166819/" class="fa fa-facebook"></a>
	            <a href="https://vk.com/cleverbel" class="fa fa-vk"></a>
	            <a href="https://www.instagram.com/cleverbelarus/" class="fa fa-instagram"></a>
	          </div>
	        </div>
	      </div>
	      <div class="catalogUL">
	      <h5>Каталог</h5>
	      <div class="hr"></div>
	        <ul>
				@foreach ($categories as $category) 
	    		<li><a href="/catalog?category_id={{ $category->id }}" class="tab">{{ $category->name }}</a></li>
	    	@endforeach	          
	        </ul>
	      </div>
			</div>
			
			<div class="center">
				<h5>Наши партнеры</h5>
	      <div class="hr"></div>
				<div class="partner-logo-container"><img style="background: white;" src="/assets/index/img/blum.jpg" alt=""></div>
				<div class="partner-logo-container"><img style="padding: 1px 3px; background: white;" src="/assets/index/img/logo-hettich-min.jpg" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding:0 3px;" src="/assets/index/img/logo-Niemann-min.png" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 7.5px 0;" src="/assets/index/img/egger_logo_1-min.jpg" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 12.5px 2px;" src="/assets/index/img/cleaf-logo-2.gif" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 3.7px 3.5px;" src="/assets/index/img/Formica_logo_svg-min.png" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 12px 0;" src="/assets/index/img/gtv-min.jpg" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 12.5px 2px;" src="/assets/index/img/SwissKrono_Logo_12-cm-min.jpg" alt=""></div>
				<div class="partner-logo-container"><img style="background: white; padding: 3px 3px;" src="/assets/index/img/part.jpg" alt=""></div>
			</div>

	    <div class="right">
	      <h5>Связаться с нами</h5>
	      <div class="hr"></div>
	      <ul class="contacts">
	        <li><img src="/assets/index/img/phone-min.png" alt="">+375 (29) 109 76 94</li>
	        <li><img src="/assets/index/img/email-min.png" alt="">cleverbelarus@gmail.com</li>
	        <li><img src="/assets/index/img/clock-min.png" alt=""><p>Ежедневно <br> c 9:00 по 20:00</p></li>
	      </ul>
	    </div>
	  </div>
	  <div class="copyright">Copyright 2015-2017&copy;cleverbel.by - Производство мебели. Работаем по Минску и Минской области. Все права защищены.</div>
	</footer>

    <a href="" class="button-go-top">
    	<p><span class="fa fa-arrow-up"></span></p>
		</a>
		
		<a href="#" data-popup-open="popup-call" class="call-button">
    	<p><span class="fa fa-phone"></span></p>
    </a>

	<div class="popup" data-popup="popup-main">
	    <div class="popup-inner">
	        <h2 class="popUpTitle">Заказать</h2>
					<p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
	        <form class="contactUs" role="form" method="post" action="/send">
	        	<input name="name" type="text" placeholder="Имя" required>
	        	<input name="email" type="text" placeholder="Email">
	        	<input name="number" type="tel" value="+375" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" placeholder="Телефон" require>
	        	<textarea name="text" placeholder="Cообщение"></textarea>
	        	<button type="submit">Заказать</button>
	        </form>
	        <a class="popup-close" data-popup-close="popup-main" href="#">x</a>
	    </div>
	</div>

	<div class="popup" data-popup="popup-call">
	    <div class="popup-inner">
	        <h2 class="popUpTitle">Заказать звонок</h2>
					<p class="popUpTitleDescr">После отправления заявки, в ближайшее время, с Вами свяжется наш менеджер.</p>
					<form class="contactUs" role="form" method="post" action="/send">
						<input style="display:none;" name="product" type="text" value="Звонок"/>
	        	<input name="name" type="text" placeholder="Имя" required>
	        	<input name="number" type="tel" value="+375" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" placeholder="Телефон" required>
	        	<textarea name="text" placeholder="Cообщение"></textarea>
	        	<button type="submit">Заказать</button>
	        </form>
	        <a class="popup-close" data-popup-close="popup-call" href="#">x</a>
	    </div>
	</div>

	<div class="popup" data-popup="popup-email">
	    <div class="popup-inner">
	        <h2 class="popUpTitle">Написать нам</h2>
			<p class="popUpTitleDescr">После отправления заявки, в ближайшее время, будет выслано сообщение на Вашу почту.</p>
			<form class="contactUs" role="form" method="post" action="/send">
				<input style="display:none;" name="product" type="text" value="Сообщение"/>
        		<input name="name" type="text" placeholder="Имя" required>
        		<input name="email" type="text" placeholder="Email" required>
        		<!-- <input name="number" type="text" placeholder="Телефон"> -->
        		<textarea name="text" placeholder="Cообщение"></textarea>
        		<button type="submit">Отправить</button>
        	</form>
	        <a class="popup-close" data-popup-close="popup-email" href="#">x</a>
	    </div>
	</div>		

	<script>
		var result;
		
		$('#flash_message').delay(2000).slideUp(400);

		window.onload = function() {
			$("html,body").animate({
			    opacity: 1
			  }, 300, function() {
			    // Animation complete.
			  });
		    result = document.getElementById('result');
				$(".fa-bars").click(function () {
					$("#mob-menu").css('left') == '0px' ? $("#mob-menu").css('left','-100%') : $("#mob-menu").css('left','0');
    		});
				$("#hide-menu").on('click', function (event) {
					event.preventDefault();
					$(".fa-bars").trigger('click');
				});
			// Если функциональность геолокации доступна, 
			// пытаемся определить координаты посетителя
			if (navigator.geolocation) {
				// Передаем две функции
				navigator.geolocation.getCurrentPosition(
				            geolocationSuccess, geolocationFailure);
				
				// Выводим результат
				result.innerHTML = "Поиск начался";
			}
			else {
				// Выводим результат
				result.innerHTML = "Ваш браузер не поддерживает геолокацию";
			}
		}

		function geolocationSuccess(position) {
			var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ position.coords.latitude + "," + position.coords.longitude + "&key=AIzaSyA4BP_mWiXqXDkC9KLwFuMq3ttp9DDP8zY";
			$.get(url, function(data, status){
		        console.log(data);
		        console.log(data.results[0]);
		       	result.innerHTML = data.results[0].address_components[3].long_name;
		    });
	
		}

		function geolocationFailure(positionError) {
			// result.innerHTML = "Ошибка геолокации";
			result.innerHTML = "Минск";
		}
		
		$(function() {
		    //----- OPEN
		    $('[data-popup-open]').on('click', function(e)  {
		        var targeted_popup_class = jQuery(this).attr('data-popup-open');
		        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
		 
		        e.preventDefault();
		    });
		 
		    //----- CLOSE
		    $('[data-popup-close]').on('click', function(e)  {
		        var targeted_popup_class = jQuery(this).attr('data-popup-close');
		        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		 
		        e.preventDefault();
		    });
		});
		$(document).on('scroll', function(event) {
			scrollPos = $(document).scrollTop();
			if(scrollPos < $(window).height()) {
				$('.button-go-top').css('opacity', '0');
				$('.call-button').css('opacity', '0');

			} else {
				$('.button-go-top').css('opacity', '1');
				$('.call-button').css('opacity', '1');
			}
		})		
		$(function() {		
			$('.button-go-top').on('click', function(event) {
				event.preventDefault();
				$('HTML, BODY').animate({scrollTop: 0}, 1000);
			})
		});

		$('.dropbtn').on('click', function (event) {
			event.preventDefault();
			var dropdown = $('#myDropdown');
			dropdown.slideToggle('slow');
		});
	</script>
</body>
</html>