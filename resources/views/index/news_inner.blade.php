@extends('index.layout')

@section('content')
<div class="flex-container">
	<div class="left-menu">
		<a href="/news"><span class="label-name"><span class="fa fa-mail-reply-all"></span> Вернуться в раздел</span></a>
	</div>
	<div class="news-block">
		<div class="photo"><img src="{{$article->image_url}}" alt=""></div>
		<div class="news-header">
			<div class="part-name">
				{{$article->name}}
			</div>
			<div class="date">
				{{$article->created_at->format('d.m.Y')}}
			</div>
		</div>
		<div class="stories">
			<p>{!! $article->description !!}</p>
		</div>
	</div>
</div>
@endsection