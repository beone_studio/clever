import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Gallery from './gallery.vue'

export default [
    {
        path: '/products',
        component: Table,
        props: {
            title: 'Товары',
            icon: 'cubes',
        },        
    }, 
    {path: '/products/create', component: Create },
    {path: '/products/:id', component: Edit },
    {path: '/products/:id/gallery', component: Gallery}       
]