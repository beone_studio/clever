import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'

export default [
    {
        path: '/news',
        component: Table,
        props: {
            title: 'Новости',
            icon: 'news',
        },        
    }, 
    {path: '/news/create', component: Create },
    {path: '/news/:id', component: Edit },      
]