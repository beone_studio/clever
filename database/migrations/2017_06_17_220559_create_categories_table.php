<?php

use App\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::dropIfExists('categories');
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable()->unique();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });

        Category::create([
            'name' => 'Детская мебель',
            'slug' => 'detskaya-mebel'
        ]);        
        Category::create([
            'name' => 'Кухни',
            'slug' => 'kuhni'
        ]);
        Category::create([
            'name' => 'Гардеробные',
            'slug' => 'garderobnye'
        ]); 
        Category::create([
            'name' => 'Шкафы-купе',
            'slug' => 'shkafy-kupe'
        ]);           
        Category::create([
            'name' => 'Торговая мебель',
            'slug' => 'torgovaya-mebel'
        ]);                     
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }       
}
