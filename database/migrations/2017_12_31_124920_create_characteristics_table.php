<?php


use App\Characteristic;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacteristicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('characteristics');
        Schema::create('characteristics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('name');
            $table->string('options')->nullable();
            $table->timestamps();
        });
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Материал',
            'options' => 'ЛДСП Egger,Kronospan'
        ]);        
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Фурнитура',
            'options' => 'Blum,Hettich,Hafelee,GTV'    
        ]);
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Система раздвижных дверей',
            'options' => 'Senator'
        ]);
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Стекло',
            'options' => 'Лакомат,Лакобель,Фотопечать,Пескоструй'    
        ]);
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Вставки',
            'options' => 'ЛДСП Egger,Kronospan'    
        ]);  
        Characteristic::create([
            'category_id' => '4',
            'name' => 'Зеркало',
            'options' => 'Серебро,Графит,Бронза'      
        ]); 
        Characteristic::create([
            'category_id' => '3',
            'name' => 'Материал',
            'options' => 'ЛДСП Egger,Kronospan'    
        ]);
        Characteristic::create([
            'category_id' => '3',
            'name' => 'Фурнитура',
            'options' => 'Blum,Hettich,Hafelee,GTV'    
        ]); 
        Characteristic::create([
            'category_id' => '2',
            'name' => 'Фасады',
            'options' => 'Пластик Alvic,ASD,Formica,ЛДСП Egger,Cleaf,Niemann,Kronospan,Крашенные из МДФ,С фрезировкой под классику'    
        ]);         
        Characteristic::create([
            'category_id' => '2',
            'name' => 'Столешницы',
            'options' => 'Постформинг Egger,Juan'    
        ]);        
        Characteristic::create([
            'category_id' => '2',
            'name' => 'Кухонный фартук',
            'options' => 'Скиналь стекло,МДФ в цвет столешницы'    
        ]); 
        Characteristic::create([
            'category_id' => '2',
            'name' => 'Фурнитура',
            'options' => 'Blum,Hettich,Hafelee,GTV'    
        ]);
        Characteristic::create([
            'category_id' => '1',
            'name' => 'Материал',
            'options' => 'ЛДСП Egger,Kronospan'     
        ]);         
        Characteristic::create([
            'category_id' => '1',
            'name' => 'Фурнитура',
            'options' => 'Blum,Hettich,Hafelee,GTV'    
        ]); 
        Characteristic::create([
            'category_id' => '5',
            'name' => 'Материал',
            'options' => 'ЛДСП Egger,Kronospan'     
        ]);         
        Characteristic::create([
            'category_id' => '5',
            'name' => 'Фурнитура',
            'options' => 'Blum,Hettich,Hafelee,GTV'    
        ]);                                                                                                          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characteristics');
    }
}
