<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{ 

    public function index()
	{
        $news = Article::get();
        return compact('news');
	}

	public function show($id) {
		$article = Article::where('id', $id)->first();
		return $article;
	} 

	public function create(Request $request) {
		Article::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'image' => $request->image->store('news', 'public'),
            'short_info' => $request->input('short_info')
        ]);
		return ['result' => 'success'];
	}

	public function update($id, Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'short_info' => $request->input('short_info')
        ];

        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($article->image);
            $data['image'] = $request->image->store('news', 'public');
        }
        $article = Article::where('id', $id)->first();
        $article->update($data);

        return ['result' => 'success'];
    }

    public function delete(Article $article)
    {
        \Storage::disk('public')->delete($article->image);
        $article->delete();

        return ['result' => 'success'];
    }	
}