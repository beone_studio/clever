<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{ 

    public function index(Request $request)
	{
		$categories = Category::get();
        $products = Product::get();
        $response = [$categories, $products];
        return compact('categories', 'products');
	}

	public function show(Product $product) {
		return $product;
	} 

	public function create(Request $request) {
        $is_by_order = $request->input('is_by_order') ? 1 : 0;
		Product::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'image' => $request->image->store('products', 'public'),
            'short_info' => $request->input('short_info'),
            'is_by_order' => $is_by_order
        ]);
		return ['result' => 'success'];
	}

	public function update(Product $product, Request $request)
    {
        $is_by_order = $request->input('is_by_order') ? 1 : 0;
        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'short_info' => $request->input('short_info'),
            'is_by_order' => $is_by_order
        ];

        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($product->image);
            $data['image'] = $request->image->store('products', 'public');
        }
        $product->update($data);

        return ['result' => 'success'];
    }

    public function delete(Product $product)
    {
        \Storage::disk('public')->delete($product->image);
        $product->delete();

        return ['result' => 'success'];
    }	

}