<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Product;
use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
    	$categories = Category::with('children')->whereNull('parent_id')->get();
        $child = Product::all()->where('category_id', 1)->first();
        $kitchen = Product::all()->where('category_id', 2)->first();
        $wordrobe = Product::all()->where('category_id', 3)->first();
        $garderob = Product::all()->where('category_id', 4)->first();
        $popular = [$child,$kitchen,$wordrobe,$garderob];
        $lastNews = Article::latest('created_at')->limit(4)->get();
        return view('index.main', compact('categories', 'popular', 'lastNews'));
    }
}
