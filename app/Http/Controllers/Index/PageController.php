<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
	public function index($page)
	{
		$article = Article::where('slug', $page)->first();
		if($article) {
			$categories = Category::with('children')->whereNull('parent_id')->get();
			return view('index.news_inner', compact('categories', 'article'));
		} else {
			abort(404);
		}
	}
}